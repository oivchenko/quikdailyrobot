
cdma_prev=nil
cdma_curr=nil


function round(num, idp)
    if num == nil then return nil end
    local mult = 10^(idp or 0)
    return math.floor(num * mult + 0.5) / mult
end

function dValue(index, v_type) --���������� �������� �������������� ��������� �����
    v_type = v_type or 'C'
    if      v_type =='O' then
        return ds:O(index)
    elseif   v_type =='H' then
        return ds:H(index)
    elseif   v_type =='L' then
        return ds:L(index)
    elseif   v_type =='C' then
        return ds:C(index)
    elseif   v_type =='V' then
        return ds:V(index)
    elseif   v_type =='M' then
        return (ds:H(index)+ds:L(index))/2
    elseif   v_type =='T' then
        return (ds:H(index)+ds:L(index)+ds:C(index))/3
    end
    return 0
end



function    dMACD(index)
    local  ema_short  = EMA_S(index, Settings.period_s, Settings.value_type)
    local  ema_long  = EMA_L(index, Settings.period_l, Settings.value_type)
    if  ema_short==nil  then    return  0 end
    if  ema_long==nil  then    return  0 end

    local   dtmp=100*(ema_short-ema_long)/ema_long
    return  round(dtmp,4)
end


function average(_start, _end, v_type)
    local sum=0
    for i = _start, _end do
        sum=sum+dValue(i, v_type)
    end
    return sum/(_end-_start+1)
end




function average_macd(_start, _end)
    local sum=0
    for i = _start, _end do
        sum=sum+ dMACD(i)
    end
    return sum/(_end-_start+1)
end




function cached_EMA()
    local cache={} --��� ������ ���������� ���������� ������� ������
    return function(ind, _p, v_t, kk)


        local n = 0 --�������� ��� � ������� �����
        local p = 0 --�������� ��� � ���������� �����
        local period = _p --������ ������� �������
        local v_type = v_t --���� ����
        local index = ind --������ (� �����)
        local k = kk or 2/(period+1) --����-� ��� ������� ���
        if index == 1 then --������ ����� - ������ �����
            cache = {} --�������������� ���
        end
        if index < period then --���� ������ ������, ��� ��������� ������ �������
            cache[index] = average(1,index, v_type) --��������� � ��� ��������� ���������� ��������
            return nil
        end
        p = cache[index-1] or dValue(index, v_type) --��������� �� ���� ���������� ��������
        n = k*dValue(index, v_type)+(1-k)*p --������� �������� ���
        cache[index] = n --� ��� ���������� ��������
        return round(n,2)
    end
end



function average_u(_start, _end, dFunc)
    local sum=0
    for i = _start, _end do
        sum=sum+dFunc(i)
    end
    return sum/(_end-_start+1)
end


function cached_EMA_UNIVERSAL(dFunc,_period)
    local cache={} --��� ������ ���������� ���������� ������� ������
    return function(ind,  kk)


        local n = 0 --�������� ��� � ������� �����
        local p = 0 --�������� ��� � ���������� �����
        local period = _period --������ ������� �������
        local index = ind --������ (� �����)
        local k = kk or 2/(period+1) --����-� ��� ������� ���
        if index == 1 then --������ ����� - ������ �����
            cache = {} --�������������� ���
        end
        if index < period then --���� ������ ������, ��� ��������� ������ �������
            cache[index] = average_u(1,index, dFunc) --��������� � ��� ��������� ���������� ��������
            return nil
        end
        p = cache[index-1] or dFunc(index) --��������� �� ���� ���������� ��������
        n = k*dFunc(index)+(1-k)*p --������� �������� ���
        cache[index] = n --� ��� ���������� ��������
        return round(n,4)
    end
end




function cached_MACD()
    local cache={} --��� ������ ���������� ���������� ������� ������
    return function(ind, _p, v_t, kk)


        local n = 0 --�������� MACD � ������� �����
        local p = 0 --�������� MACD � ���������� �����
        local period = _p --������ ������� �������
        local v_type = v_t --���� ����
        local index = ind --������ (� �����)
        local k = kk or 2/(period+1) --����-� ��� ������� ���
        if index == 1 then --������ ����� - ������ �����
            cache = {} --�������������� ���
        end
        if index < period then --���� ������ ������, ��� ��������� ������ �������
            cache[index] =  average_macd(1,index) --��������� � ��� ��������� ���������� ��������
            return nil
        end
        p = cache[index-1] or dMACD(index) --��������� �� ���� ���������� ��������
        n = k*dMACD(index)+(1-k)*p --������� �������� ���
        cache[index] = n --� ��� ���������� ��������
        return round(n,4)
    end
end





function    LLV(_ind)
        local period = Settings.sth_n --������ ������� �������
        local index = _ind --������ (� �����)
        local v_type = 'L' --���� ����

        local   rslt=dValue(index,v_type)
        if  index==1
        then
            return  rslt
        end


        local   _end=index-1
        local   _start=index-period
        local   _crnt
        if  _start<1    then    _start=1    end
        if  _end<1      then    _end=1      end


        for i = _start, _end do
            _crnt=dValue(i,v_type)
            if  _crnt<rslt  then    rslt=_crnt  end
        end

        return  rslt
end



function    HHV(_ind)
    local period = Settings.sth_n --������ ������� �������
    local index = _ind --������ (� �����)
    local v_type = 'H' --���� ����

    local   rslt=dValue(index,v_type)
    if  index==1
    then
        return  rslt
    end


    local   _end=index-1
    local   _start=index-period
    local   _crnt
    if  _start<1    then    _start=1    end
    if  _end<1      then    _end=1      end


    for i = _start, _end do
        _crnt=dValue(i,v_type)
        if  _crnt>rslt  then    rslt=_crnt  end
    end

    return  rslt
end


function    SMA(valF)
    return  function(_indx)
                local   _period=Settings.sth_m
                local   _end=_indx
                local   _start=_end-_period+1
                local   _crnt=0
                if  _start<1    then    _start=1    end
                if  _end<1      then    _end=1      end
                for i=_start,_end
                do
                    _crnt=_crnt+valF(i)
                end
                if  _start~=_end    then    _crnt=_crnt/(_end-_start) end
                return  round(_crnt,4)
            end
end


local   _f01=function(_indx) return (dValue(_indx,'C')-LLV(_indx)) end
local   _f02=function(_indx) return (HHV(_indx)-LLV(_indx)) end
local   SMA01=SMA(_f01)
local   SMA02=SMA(_f02)

function    STAH_K(_indx)
            local   _rslt=100*SMA01(_indx)/SMA02(_indx)
            return  round(_rslt,4)
end


STAH_D=cached_EMA_UNIVERSAL(STAH_K,Settings.sth_m)



function  cbb(index)
    cdma_curr  = myMACD(index, Settings.period_macd, Settings.value_type)
    macd_curr = dMACD(index)

    if index>Settings.distance
    then
        cdma_prev  = myMACD(index-Settings.distance, Settings.period_macd, Settings.value_type)
        macd_prev = dMACD(index-Settings.distance)
    end

--    local msg0="i="..index.."  razn="..round(cdma_curr-cdma_prev,4).."  ma(macd)_curr="..cdma_curr.." ma(macd)_prev="..cdma_prev
    local msg0="i="..index.."  razn="..round(macd_curr-macd_prev,4).."  macd_curr="..macd_curr.." macd_prev="..macd_prev
    AddMess(msg0)


--    local msg1="i="..index.."  STH_K="..STAH_K(index).." STH_D="..STAH_D(index)
--    local msg1="i="..index.."  STH_K="..STAH_K(index)

    local msg1="i="..index.."  LLV="..LLV(index).."  HHV="..HHV(index).."  _f01=".._f01(index).."  _f02=".._f02(index).."  STH_K="..STAH_K(index)
    AddMess(msg1)

--     if  1==1 or robot_is_run
    if  robot_is_run
    then
    end
end




