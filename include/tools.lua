function round(num, idp)
    if num == nil then return nil end
    local mult = 10^(idp or 0)
    return math.floor(num * mult + 0.5) / mult
end



function dValue(index, v_type) --���������� �������� �������������� ��������� �����
    v_type = v_type or 'C'
    if      v_type =='O' then
        return ds:O(index)
    elseif   v_type =='H' then
        return ds:H(index)
    elseif   v_type =='L' then
        return ds:L(index)
    elseif   v_type =='C' then
        return ds:C(index)
    elseif   v_type =='V' then
        return ds:V(index)
    elseif   v_type =='M' then
        return (ds:H(index)+ds:L(index))/2
    elseif   v_type =='T' then
        return (ds:H(index)+ds:L(index)+ds:C(index))/3
    end
    return 0
end




function average(_start, _end, _dFunc)
    local sum=0
    for i = _start, _end do
        sum=sum+_dFunc(i)
    end
    return sum/(_end-_start+1)
end





function cached_EMA(_period,_type,_dFunc)
    local cache={} --��� ������ ���������� ���������� ������� ������
    return function(ind, kk)


        local n = 0 --�������� ��� � ������� �����
        local p = 0 --�������� ��� � ���������� �����
        local period = _period --������ ������� �������
        local v_type = _type --���� ����
        local index = ind --������ (� �����)
        local k = kk or 2/(period+1) --����-� ��� ������� ���
        if index == 1 then --������ ����� - ������ �����
            cache = {} --�������������� ���
        end
        if index < period then --���� ������ ������, ��� ��������� ������ �������
            cache[index] = average(1,index, _dFunc) --��������� � ��� ��������� ���������� ��������
            return nil
        end
        p = cache[index-1] or _dFunc(index) --��������� �� ���� ���������� ��������
        n = k*_dFunc(index)+(1-k)*p --������� �������� ���
        cache[index] = n --� ��� ���������� ��������
        return round(n,2)
    end
end





function    getEMA(_period,_type)
    local  dfunc=function(indx)  return dValue(indx,_type) end
    local  ema_0  = cached_EMA( _period, _type,dfunc)

    return 	function(index)
        local   dtmp=ema_0(indx)
        return  round(dtmp,4)
    end
end








function    getMACD(_nshort,_nlong,_type)
    local  dfunc=function(indx)  return dValue(indx,_type) end
    local  ema_s  = cached_EMA( _nshort, _type,dfunc)
    local  ema_l  = cached_EMA( _nlong, _type,dfunc)

    return 	function(index)
    			local  ema_short  = ema_s(index)
		    	local  ema_long  = ema_l(index)
		    	if  ema_short==nil  then    return  0 end
		    	if  ema_long==nil  then    return  0 end

		    	local   dtmp=100*(ema_short-ema_long)/ema_long
		    	return  round(dtmp,4)
		end
end










function    getLLV(_sth_n)
return	function(_ind)
        local period = _sth_n --������ ������� �������
        local index = _ind --������ (� �����)
        local v_type = 'L' --���� ����

        local   rslt=dValue(index,v_type)
        if  index==1
        then
            return  rslt
        end


        local   _end=index-1
        local   _start=index-period
        local   _crnt
        if  _start<1    then    _start=1    end
        if  _end<1      then    _end=1      end


        for i = _start, _end do
            _crnt=dValue(i,v_type)
            if  _crnt<rslt  then    rslt=_crnt  end
        end

        return  rslt
end
end



function    getHHV(_sth_n)
return	function(_ind)
    local period = _sth_n --������ ������� �������
    local index = _ind --������ (� �����)
    local v_type = 'H' --���� ����

    local   rslt=dValue(index,v_type)
    if  index==1
    then
        return  rslt
    end


    local   _end=index-1
    local   _start=index-period
    local   _crnt
    if  _start<1    then    _start=1    end
    if  _end<1      then    _end=1      end


    for i = _start, _end do
        _crnt=dValue(i,v_type)
        if  _crnt>rslt  then    rslt=_crnt  end
    end

    return  rslt
end
end


function    getSMA(valF,_sth_m)
    return  function(_indx)
                local   _period=_sth_m
                local   _end=_indx
                local   _start=_end-_period+1
                local   _crnt=0
                if  _start<1    then    _start=1    end
                if  _end<1      then    _end=1      end
                for i=_start,_end
                do
                    _crnt=_crnt+valF(i)
                end
                if  _start~=_end    then    _crnt=_crnt/(_end-_start) end
                return  round(_crnt,4)
            end
end





function	get_STAH_K(_sth_n,_sth_m)

local	LLV=getLLV(_sth_n)
local	HHV=getHHV(_sth_n)
local   _f01=function(_indx) return (dValue(_indx,'C')-LLV(_indx)) end
local   _f02=function(_indx) return (HHV(_indx)-LLV(_indx)) end
local   SMA01=getSMA(_f01,_sth_m)
local   SMA02=getSMA(_f02,_sth_m)


return	function(_indx)
	    
            local   _rslt=100*SMA01(_indx)/SMA02(_indx)
            return  round(_rslt,4)
end



end









function  cbb(index)

    macd_curr  = myMACD(index)
    macd_prev  = myMACD(index)


    if index>Settings.distance
    then
        macd_prev = myMACD(index-Settings.distance)
    end
    macd_razn=round(macd_curr-macd_prev,4)	


    local	lbl_macd_curr="macd_curr"
    local	lbl_macd_prev="macd_prev(-"..Settings.distance..")"

    local msg0="i="..index.."  razn="..macd_razn.."  "..lbl_macd_curr.."="..macd_curr.." "..lbl_macd_prev.."="..macd_prev
    AddMess(msg0)



    sth_k_curr=mySTAH_K(index)
--    local msg1="i="..index.."  LLV="..LLV(index).."  HHV="..HHV(index).."  _f01=".._f01(index).."  _f02=".._f02(index).."  STH_K="..STAH_K(index)
    local msg1="i="..index.."  STH_K="..sth_k_curr
    AddMess(msg1)

--     if  1==1 or robot_is_run
    if  robot_is_run
    then
    end
end




