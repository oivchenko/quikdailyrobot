package.path = package.path .. ";" .. getScriptPath() .. "\\?.lua"
package.cpath = package.cpath .. ";" .. getWorkingFolder() .. "\\?51.dll"


-- package.cpath = package.cpath .. ";" .. getScriptPath() .. "\\clibs\\?.dll"
-- package.path = package.path .. ";" .. getScriptPath() .. "\\lua\\?.lua"


require "QL"
require "settings"
--require "emamod" 

dofile(getScriptPath().."\\include\\tools.lua")


is_run = true
robot_is_run =false 
in_transaction=false

operation=nil
trans_id=nil
order_num=nil


curr_block=0

local tm_prev=os.clock()
local tm_curr=os.clock()
local wlog=nil


local   fcl_record=nil
local   fch_record=nil



function  AddMess(msg)
--[[
    if wlog:IsClosed()
    then
        wlog:Show()
    end

    local row=wlog:AddLine()
    wlog:SetValue(row,"date",os.date())
    wlog:SetValue(row,"message",msg)

    wlog:Highlight(row,nil,RED,nil,500)
    ]]
    message(msg,1)
    toLog(log,msg)

end



function main()
	if isConnected()
    then
	    InitEMA()
	    StopRobot()
	end

    while is_run do
	    tm_curr=os.clock()
	    step()
        sleep(50)
    end
end




function OnInit()

    message(" interval >>  "..Settings.interval,2)

--[[
	wlog = QTable.new()
 	wlog:AddColumn("date", QTABLE_STRING_TYPE, 20)
  	wlog:AddColumn("message", QTABLE_STRING_TYPE, 100)
	wlog:SetCaption("Log Window")
   	wlog:Show()


]]

end


function InitEMA()

    myMACD=getMACD(Settings.block_1.alg_1.Nshort,Settings.block_1.alg_1.Nlong,Settings.value_type)
    mySTAH_K=get_STAH_K(Settings.block_1.alg_2.sth_n,Settings.sth_m)

    myMACD_b2=getMACD(Settings.block_2.alg_1.Nshort,Settings.block_2.alg_1.Nlong,Settings.value_type)
    myEMA_b2=getEMA(Settings.block_2.alg_2.period,Settings.value_type)


        tm_prev=os.clock()
        tm_curr=os.clock()
        ds,err=CreateDataSource(Settings.class_code,Settings.sec_code,  Settings.interval )
        assert(ds,err)

       
--        AddMess("Settings > "..table2string(Settings))
--        AddMess(ds)
        res=ds:SetUpdateCallback(cbb)
end


function StopRobot()
	robot_is_run=false
	tm_prev=os.clock()
	tm_curr=os.clock()
    in_transaction=false
    trans_id=nil
end

function OnConnected()
    AddMess("Connect")
	InitEMA()
	StopRobot()
end

function OnDisconnected()
    AddMess("Disconnect")
	robot_is_run=false
    in_transaction=false
	tm_prev=os.clock()
	tm_curr=os.clock()
end


function step()

    if not isConnected()
    then
        return
    end

  	if robot_is_run
    then
        if (tm_curr-tm_prev)<Settings.timeout_trans
        then
            return
        end
    else
        if (tm_curr-tm_prev)<Settings.delay_start_robot
        then
            return
        end
        robot_is_run=true
        AddMess("Robot started")
	end


--    tm_prev=tm_curr
--    reget_futures_holding()
--   reget_futures_limits()
    if not is_run  then    return end
--    maketrans()

end



function OnStop()
--    wlog:delete()
    is_run = false
end

--[[

function  OnFuturesLimitChange(tbl)
    if  in_transaction  then    return end
    AddMess("OnFuturesLimitChange >> "..table2string(tbl))
    local evntmsg="OnFuturesLimitChange trdaccid(account)="..tbl.trdaccid.."  limit_type="..
        tbl.limit_type.."     varmargin="..tbl.varmargin.."  accruedint=".. tbl.accruedint..
        "  cbplused="..tbl.cbplused
    AddMess(evntmsg)
    restart_cycle()
end

function  OnFuturesClientHolding(tbl)
    if  in_transaction  then    return end

    AddMess("OnFuturesClientHolding >> "..table2string(tbl))
    local evntmsg="OnFuturesClientHolding trdaccid(account)="..tbl.trdaccid.."  type="..
        tbl.type.."     totalnet="..tbl.totalnet
    AddMess(evntmsg)
    restart_cycle()
end

function  OnTransReply(tbl)
    if not in_transaction        then    return  end
   if tbl.trans_id==trans_id and tbl.status==3 then
        order_num=tbl.order_num
        AddMess("OnTransReply >> "..table2string(tbl))
   end
end

function  OnOrder(tbl)
    AddMess("OnOrder >> "..table2string(tbl))
end

function  OnTrade(tbl)

   AddMess("OnTrade >> "..table2string(tbl))

   if not in_transaction        then    return  end
   if trans_id==nil             then    return  end
   if order_num==nil            then    return end
   if order_num~=tbl.order_num  then    return  end



   local    prm_operation="B"
   if operation=="B"    then    prm_operation="S" end


   proskal_tp=Settings.proskal_tp
   proskal_sl=Settings.proskal_sl

   offset=Settings.offset
   spread=Settings.spread




                --       ema_curr    -- ??????????    onTrade
   stopprice=tbl.price-proskal_tp
   stopprice2=tbl.price+proskal_sl
   price=stopprice2+Settings.otstup

   if prm_operation=="S"
   then
       stopprice=tbl.price+proskal_tp
       stopprice2=tbl.price-proskal_sl
       price=stopprice2-Settings.otstup
   end

   local    units="PERCENTS"
   if  Settings.units==0
   then
       units="PRICE_UNITS"
   end

_,rslt = sendTakeProfitAndStopLimit(
    Settings.class_code ,       --  class
    Settings.sec_code ,         --  security
        prm_operation ,             --  direction
        price ,                         --  price
        stopprice ,                         --  stopprice
        stopprice2 ,                       --  stopprice2
        Settings.quantity ,         --  volume
        offset ,                         --  offset
        units ,                --  offsetunits
        spread ,                         --  deffspread
        units ,                --  deffspreadunits
        Settings.account ,          --  account
        "TODAY" ,                   --  exp_date
        nil ,                       --  client_code
        nil                         --  comment
   )

   AddMess(">>>>>     stopprice="..toPrice(Settings.sec_code,stopprice,Settings.class_code).."     stopprice2="..toPrice(Settings.sec_code,stopprice2,Settings.class_code))

    AddMess( rslt )

    trans_id=nil
    order_num=nil
   in_transaction=false

    restart_cycle()

end


]]


function    search_futures_limits(t)
    return  t.trdaccid==Settings.account and t.limit_type==0
end

function    search_futures_holding(t)
    return  t.trdaccid==Settings.account   and t.type==0
end

function    reget_futures_holding()
    AddMess("reget_futures_holding  !!!  ")
    fch_record=nil
    nrecs=getNumberOf("futures_client_holding")
    if nrecs==0 then
        AddMess(" futures_client_holding  no records " )
        return
    end

    recs=SearchItems("futures_client_holding",0, nrecs-1,search_futures_holding)
    if recs==nil
    then
        AddMess(" futures_client_holding by filter no records ")
        return
    end

    fch_record=getItem("futures_client_holding",recs[1])
    if fch_record==nil
    then
        AddMess(" futures_client_holding  fch_record=nil ")
        return
    end

    local msg0="fch_record  totalnet="..fch_record.totalnet
    AddMess(msg0)
    AddMess("futures_client_holding fch_record="..table2string(fcl_record) )
end

function    reget_futures_limits()
    fcl_record=nil

    nrecs=getNumberOf("futures_client_limits")
    if nrecs==0 then
        AddMess(" futures_client_limits  no records " )
        return
    end

    recs=SearchItems("futures_client_limits",0, nrecs-1,search_futures_limits)
    if recs==nil
    then
        AddMess(" futures_client_limits by filter no records ")
        return
    end

    fcl_record=getItem("futures_client_limits",recs[1])
    if fcl_record==nil
    then
        AddMess(" futures_client_limits  fcl_record=nil ")
        return
    end

    local msg0="fcl_record  cbplused="..fcl_record.cbplused.."   varmargin="..fcl_record.varmargin.."     accruedint="..fcl_record.accruedint
    AddMess(msg0)
    AddMess("futures_client_limits fcl_record="..table2string(fcl_record) )


    local  sm=fcl_record.varmargin + fcl_record.accruedint

    if sm < Settings.sum_min
    then
        is_run=false
        AddMess("Control block "..fcl_record.varmargin.." + "..fcl_record.accruedint.." < "..Settings.sum_min)
        return
    end

    if sm > Settings.sum_max
    then
        is_run=false
        message("Control block "..fcl_record.." + "..fcl_record.accruedint.." > "..Settings.sum_max)
        return
    end
end




function    restart_cycle()
    AddMess(" restart cycle")
    tm_prev=tm_curr-Settings.timeout_trans+2
end



function	maketrans()

    if  in_transaction and trans_id~=nil and order_num==nil
    then
        _,lrez=killAllOrders({ACCOUNT=Settings.account})
        AddMess("Kill order !!!!")
        AddMess(lrez)
        in_transaction=false
        trans_id=nil
    end


--    if fch_record==nil then return end
    if  (not in_transaction) and (fch_record~=nil) and  (fch_record.totalnet~=0)  then   return end

    if  ema_curr==nil or ema_prev==nil  then    return  end

    local p_delta=ema_curr-ema_prev
    local ap_delta=math.abs(p_delta)
    local s_delta=math.abs(Settings.s_delta)

    if  ap_delta<s_delta
    then
        AddMess(" p_delta="..p_delta.."      s_delta="..s_delta)
        return
    end


    local msg=" ema_prev="..ema_prev.." ema_curr="..ema_curr
    AddMess(msg)

    local price=ema_curr

    if (p_delta<0) then
            msg=" Sale "..msg
            operation="S"
            price=price-Settings.proskal
    end
    if (p_delta>0) then
            msg=" Buy "..msg
            operation="B"
            price=price+Settings.proskal
    end

     trans_id=random_max()
     trans={}
     trans["ACCOUNT"]     = Settings.account
     trans["CLIENT_CODE"] = Settings.account
     trans["TRANS_ID"]    = tostring(trans_id)
     trans["CLASSCODE"]   = Settings.class_code
     trans["SECCODE"]     = Settings.sec_code
     trans["ACTION"]      = "NEW_ORDER"
     trans["OPERATION"]   = operation
     trans["QUANTITY"]    = tostring(Settings.quantity)
     trans["TYPE"]    = "M"
     trans["PRICE"]   = toPrice(Settings.sec_code,price,Settings.class_code)


        local   lkod=sendTransaction(trans)

        local mess="\n"..
                "ACCOUNT="..trans["ACCOUNT"].."\n"..
                "CLIENT_CODE="..trans["CLIENT_CODE"].."\n"..
                "TRANS_ID="..trans["TRANS_ID"].."\n"..
                "CLASSCODE="..trans["CLASSCODE"].."\n"..
                "SECCODE="..trans["SECCODE"].."\n"..
                "ACTION="..trans["ACCOUNT"].."\n"..
                "OPERATION="..trans["OPERATION"].."\n"..
                "QUANTITY="..trans["QUANTITY"].."\n"..
                "TYPE="..trans["TYPE"].."\n"..
                "PRICE="..trans["PRICE"].."\n".."\n"..
                "seconds="..(os.clock()-tm_prev).."\n".."\n"..
                "Result="..lkod

        AddMess(mess)

        if lkod==nil  then  lkod=""   end
        if trim(lkod)==trim("")
        then
            in_transaction=true
            order_num=nil
        else
            restart_cycle()
        end

end

